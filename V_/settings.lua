data:extend({
	{
		type = "bool-setting",
		setting_type = "runtime-global",
		default_value = true,
		name = "Module-Admin",
		order = "a"
	},
	{
		type = "bool-setting",
		setting_type = "runtime-global",
		default_value = true,
		name = "Module-Cheats",
		order = "b"
	},
	{
		type = "int-setting",
		setting_type = "runtime-global",
		default_value = 10,
		minimum_value = 0,
		maximum_value = 100,
		name = "TP_History_Length",
		order = "b1"
	},
	{
		type = "bool-setting",
		setting_type = "runtime-global",
		default_value = true,
		name = "Module-QoL",
		order = "c"
	},
	{
		type = "bool-setting",
		setting_type = "runtime-global",
		default_value = true,
		name = "Adjust-walkspeed-to-GameSpeed",
		order = "ca"
	},
	{
		type = "bool-setting",
		setting_type = "runtime-global",
		default_value = false,
		name = "Module-Teleport-Points",
		order = "d"
	}
})