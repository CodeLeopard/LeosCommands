--[[ Copyright (C) 2019  CodeLeopard

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License version 3
    along with this program.  If not, see https://opensource.org/licenses/gpl-3.0
]]


local Lib = {}
Lib.Version = "0.2.1"
Lib.Name = "Lib"

myCommands = {}

--[[Allows checking if a table has a value without crashing the game, beware of performance: this is not fast!]]
function Lib.GetMemberSafe(table, key)
	local call_result, value = pcall( function () return table[key] end )
	if call_result then
		return value
	else
		return nil
	end
end

--[[Resets permission groups if not defined, or when force is specified (version 1.1)]]
function Lib.ResetPermissionGroups(force)
	if not global.ElevatedPermissionGroups then
		global.ElevatedPermissionGroups = {};
		log("Lib.ResetPermissionGroups: Created PermissionGroupTable.");
	elseif force then
		global.ElevatedPermissionGroups = {};
		log("Lib.ResetPermissionGroups: Force-Reset PermissionGroupTable.");
	else
		log("Lib.ResetPermissionGroups: Checked PermissionGroupTable.");
	end
end


--[[Check if player has permjssion to use privileged commands: if allowed returns true (version 1.0)]]
function Lib.CheckPermission(player)
	if player.admin then return true end
	Lib.ResetPermissionGroups() --[[Will check if the groups are valid, and reset them if they are not.]]
	if global.ElevatedPermissionGroups[player.permission_group.name] then
		return true
	end

	--[[When we get there there was nothing that grants permission]]
	return false
end


--[[Edited from https://stackoverflow.com/questions/28664139/lua-split-string-into-words-unless-quoted]]
--[[Separates words from given string but preserves quoted sections (version 1.1)]]
function Lib.SmartWordSeparation(text)
	local spat, epat, buf, quoted = [=[^(['"])]=], [=[(['"])$]=], nil, nil
	local separatedStrings = {}
	for str in text:gmatch("%S+") do
		local squoted = str:match(spat)
		local equoted = str:match(epat)
		local escaped = str:match([=[(\*)['"]$]=])
		if squoted and not quoted and not equoted then
			buf, quoted = str, squoted
		elseif buf and equoted == quoted and #escaped % 2 == 0 then
			str, buf, quoted = buf .. ' ' .. str, nil, nil
		elseif buf then
			buf = buf .. ' ' .. str
		end
		if not buf then
			--[[print((str:gsub(spat,""):gsub(epat,"")))]]
			table.insert(separatedStrings, (str:gsub(spat,""):gsub(epat,"")))
		end
	end

	if buf then
		--[[print("Missing matching quote for ".. buf)]]
		return nil
	end
	--[[log(serpent.block(separatedStrings))]]
	return separatedStrings
end

--[[(version 1.0)]]
function Lib.RunFunctionsInTable(table, event)
	for key, func in pairs(table) do
		func(event)
	end
end

--[[(version 2.0)]]
--entity to tp, destination, [destination surface], [search radius], [search step]
--returns: false for fail, true for success
function Lib.SafeTP(entity, position, surface, searchRad, searchStep)
	if not entity or not entity.valid or not position then
		log("SafeTP: invalid arguments!" .. serpent.block {entity, position, surface, searchRad, searchStep })
		return false
	end

	surface = surface or entity.surface
	searchRad  = searchRad or 10
	searchStep = searchStep or 0.1
	local prototype = Lib.GetMemberSafe(entity, "prototype")

	if not prototype then --Entity is actually a player...
		if entity.character then
			prototype = entity.character.prototype
		else --Player is in ghost/GodMode
			entity.teleport(position, surface)
			return true
		end
	end

	local position2 = surface.find_non_colliding_position(prototype.name, position, searchRad, searchStep)
	if not position2 then
		-- No safe destination.
		 return false
	elseif surface == entity.surface then
		entity.teleport(position2)
		return true
	else
		log("SafeTP: Cannot tp non-player entity across surfaces.")
		return false
	end
end


function Lib.SafeEventPlayer(event)
	local evindex = event.player_index
	if evindex == nil then
		--log("Notice: event.player_index was nil, this happens when the server console issues a command")
		return true, Lib.FakePlayer_Server()
	else
		local index = tonumber(evindex)
		if index == nil then
			log("Error: event.player_index was not a number!\n" .. serpent.block(event)) -- to ensure it prints, whatever it is.
			return false, nil
		else
			local player = game.players[event.player_index]
			if player then
				return false, player
			else
				log("Error Player not found from event\n" .. serpent.block(event))
				return false, nil
			end
		end
	end
end

function Lib.FakePlayer_Server()
	local player = {
		admin = true,
		name = "Server Console",
		print = function(string) log("Server>" .. string) end
	}
	return player
end




Lib.ConsoleCommands = myCommands
--log("Fullread")
return Lib