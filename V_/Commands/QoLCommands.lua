--[[ Copyright (C) 2019  CodeLeopard

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License version 3
    along with this program.  If not, see https://opensource.org/licenses/gpl-3.0
]]


local Lib = require("CommandsLib")

local QoL = {}
QoL.Version = "0.2.1"
QoL.Name = "QoL"

local myCommands = {}

function QoL.Position(event)
	local fake, player = Lib.SafeEventPlayer(event)
	if not player then
		log("Error: Command/Position: SafePlayer was nil")
		return
	end
	if fake then
		player.print("This command can't be used from the server console.")
		return
	end
	player.print("Your position is: x" .. player.position.x .. " y" .. player.position.y)
end
myCommands["Position"] =
{
	help = "Prints your current position [Anyone]",
	func = QoL.Position,
	version = "1.0"
}


function QoL.NameOfItem (event)
	local fake, player = Lib.SafeEventPlayer(event)
	if not player then
		log("Error: Command/NameOfItem: SafePlayer was nil")
		return
	end
	if fake then
		player.print("This command can't be used from the server console.")
		return
	end

	if player.cursor_stack.valid_for_read then
		player.print("Cursor item name: \"" .. player.cursor_stack.name .. "\".")
		player.print("Cursor item subgroup: \"" .. player.cursor_stack.prototype.subgroup.name .."\", order: \"" .. player.cursor_stack.prototype.order .. "\"." )
	else
		player.print("Cursor is empty.")
	end

	if player.selected then
		player.print("Selected thing's name: \"" .. player.selected.name .. "\".")
		player.print("Selected thing's type: \"" .. player.selected.type .. "\".")
	else
		player.print("There was nothing selected.")
	end
end
myCommands["NameOfItem"] =
{
	help = "Prints information about the item on your cursor and/or the thing you have selected. [Anyone]",
	func = QoL.NameOfItem,
	version = "1.3",
	aliases = {"NameThis","NameOf",}
}


function QoL.OnGameSpeedChanged()
	local walkSpeedMod = global.WalkSpeedModifier
	if not walkSpeedMod then
		global.WalkSpeedModifier = 1
		walkSpeedMod = 1
	end

	local modifier = 1
	local adjust = settings.global["Adjust-walkspeed-to-GameSpeed"].value
	--log(serpent.line(adjust))
	if adjust then
		modifier = ((walkSpeedMod/game.speed) - 1)
		--log("adjus " .. modifier)
	else
		--log("unadj " .. modifier)
		modifier = walkSpeedMod - 1
	end
	if modifier < -0.9 then modifier = -0.9 end
	--log(modifier)
	game.forces["player"].character_running_speed_modifier = modifier
end
--[[OnGameSpeedChanged (version 1.0)]]


function QoL.WalkSpeed(event)
	local fake, player = Lib.SafeEventPlayer(event)
	if not player then
		log("Error: Command/WalkSpeed: SafePlayer was nil")
		return
	end

	local inputString = event["parameter"]
	local walkSpeedMod = global.WalkSpeedModifier
	if not walkSpeedMod then
		walkSpeedMod = 1
		global.WalkSpeedModifier = 1
	end


	log(serpent.block(event))


	if not inputString then
		player.print("The walkingSpeed modifier is "..walkSpeedMod.. ".")
		player.print("It is applied like so: running_speed_bonus = (modifier=" ..walkSpeedMod..  "/game.speed=" ..game.speed.. ") - 1).")
		player.print("The walkingSpeed is adjusted like that to make the effect of slowing down the game less noticable.")
		return
	end

	if not player.admin then
		player.print("You don't have permission to use this command.")
		return
	end

	if inputString == "help" then
		player.print("Use \"/walkspeed [new value]\" to edit the walkingSpeed modifier. It is applied like so: running_speed = (modifier/game.speed) - 1).")
		player.print("The default is 1. Valid values range from >0 to <=100. 0.5 means the speed is halved, 2 means the speed is doubled.")
		return
	end


	local newValue = tonumber(inputString)
	if not newValue then player.print(inputString .. " could not be onverted to a number!") return end
	if not (newValue > 0) then player.print("The newvalue must be > 0.") return end
	if not (newValue < 100) then player.print("The newvalue must be < 100.") return end

	--[[The newValue is correct.]]
	global.WalkSpeedModifier = newValue
	QoL.OnGameSpeedChanged()
	game.print(player.name .. " changed the walkingSpeedModifier to " .. newValue .. ".")
	return
end
myCommands["WalkSpeed"] =
{
	help = "[Anyone] see the how the walking speed is adjusted. [Admin] use subcommand \"help\" for additional information.",
	func = QoL.WalkSpeed,
	version = "1.1",
}


function QoL.OnSpeedBoundsChanged(player)
	local lower = global.GameSpeedLowerBound
	local upper = global.GameSpeedUpperBound

	if not game or not game.speed then return end

	if game.speed > upper then
		game.speed = upper
		QoL.OnGameSpeedChanged()
		game.print(player.name .. " changed the GameSpeed bounds and the GameSpeed has been changed to " .. game.speed .. " to comply.")
		return
	end
	if game.speed < lower then
		game.speed = lower
		QoL.OnGameSpeedChanged()
		game.print(player.name .. " changed the GameSpeed bounds and the GameSpeed has been changed to " .. game.speed .. " to comply.")
		return
	end
end
--[[OnSpeedBoundsChanged (version 1.2)]]


function QoL.ResetSpeedBounds(player)
	global.GameSpeedLowerBound = 0.05
	global.GameSpeedUpperBound = 1
	QoL.OnSpeedBoundsChanged(player)
end
--[[ResetSpeedBounds (version 1.1)]]


function QoL.GameSpeedChange(player, newSpeed)
	local speedLowerBound = global.GameSpeedLowerBound
	local speedUpperBound = global.GameSpeedUpperBound

	if not speedLowerBound or not speedUpperBound then QoL.ResetSpeedBounds(player) log("Command/GameSpeed: Speedbounds ware reset because they ware nil!") end

	if newSpeed >= 9000 and newSpeed > speedUpperBound then player.print("The Server's Power Level can't handle that kind of Speed!") return end
	if newSpeed > speedUpperBound then player.print("You may not set the GameSpeed to above " .. speedUpperBound .. ".") return end
	if newSpeed == 0 then player.print("Don't try to crash the Server! Setting GameSpeed to zero makes no sense!") return end
	if newSpeed <  0 then player.print("Don't try to crash the Server! Setting GameSpeed to negative makes no sense!") return end
	if newSpeed < 0.05 then player.print("You can't set the GameSpeed to below 0.05: it is too slow!") return end
	if newSpeed < speedLowerBound then player.print("You may not set the GameSpeed to below " .. speedLowerBound .. ".") return end

	log("Command/GameSpeed: We will apply " .. newSpeed .. " as the new GameSpeed.")
	game.speed = newSpeed
	QoL.OnGameSpeedChanged()
	game.print(player.name .. " set the game speed to " .. newSpeed)
end
--[[GameSpeedChange (version 1.1)]]


function QoL.GameSpeedBoundChange(player, lower, upper)
	if lower < 0.05  then
		player.print("The lowerBound must be >= 0.05, the game cannot handle speeds below that in multiplayer.")
		return
	end
	if lower > upper then
		player.print("The lowerBound must be less than the upperBound.")
		return
	end

	--[[Nothing was wrong]]
	global.GameSpeedLowerBound = lower
	global.GameSpeedUpperBound = upper
	player.print("Bounds are now: >= " .. lower .. " and <=  " .. upper .. ".")
	QoL.OnSpeedBoundsChanged(player)
end
--[[GameSpeedBoundChange (version 1.1)]]


function QoL.GameSpeed(event)
	local fake, player = Lib.SafeEventPlayer(event)
	if not player then
		log("Error: Command/GameSpeed: SafePlayer was nil")
		return
	end

	local inputString = event["parameter"]


	local speedLowerBound = global.GameSpeedLowerBound
	local speedUpperBound = global.GameSpeedUpperBound



	if not inputString then
		player.print("The game speed is currently set to: " .. game.speed .. ". This means " .. (60 * game.speed) .. " UPS, wich is " .. 100 * game.speed .. "%")
		return
	end

	log("Command/GameSpeed: ".. player.name .. " entered \"" .. inputString .. "\" as arguments.")

	if inputString == "help" then
		if not Lib.CheckPermission(player) then
			player.print("UPS is Updates Per Second. 60 UPS (GameSpeed 1) is normal. Your FPS is always less than (or equal to) UPS.")
			player.print("The game may be slowed down to allow people on weaker computers to keep playing.")
			return
		end
		--[[User is privileged]]
		player.print("Use \"/gamespeed [number]\" to change the game speed. Bounds are: >= " .. speedLowerBound .. " and <= " .. speedUpperBound .. "." )
		player.print("Use \"/gamespeed slowest\" to set the GameSpeed to the slowest allowed value.")
		if player.admin then
			player.print("To change the bounds type \"/gamespeed bounds [lower] [upper]\" and replace the \"[...]\" with the appropriate values.")
			player.print("To reset the bounds and back to defaults type \"/gamespeed reset\".")
		end
		return
	end

	if not Lib.CheckPermission(player) then
		player.print("You don't have permission to change the GameSpeed.")
		return
	end

	inputArgs = Lib.SmartWordSeparation(inputString)
	--[[Check if the command is to change the speed bounds]]

	local newSpeed = tonumber(inputString)

	if inputArgs[1] == "bounds" then
		if not player.admin then player.print("You need to be an Admin to change the speed bounds.") return end

		local newLower = tonumber(inputArgs[2])
		local newUpper = tonumber(inputArgs[3])
		if newLower and newUpper then QoL.GameSpeedBoundChange(player, newLower, newUpper) return
		else
			player.print("Arguments could not be converted to a number!")
			return
		end
	elseif inputArgs[1] == "reset" then
		QoL.ResetSpeedBounds(player)
		player.print("Reset speedbounds to defaults.")
		return
	elseif inputArgs[1] == "slowest" then
		QoL.GameSpeedChange(player, speedLowerBound)
		return
	elseif inputArgs[1] and not newSpeed then
		player.print("Unknown argument(s) \"" .. inputString .. "\"")
		return
    end

	if not newSpeed then player.print(inputString .. " could not be converted into a number!") return end
	QoL.GameSpeedChange(player, newSpeed)
end
myCommands["GameSpeed"] =
{
	help = "[Anyone] Show the current GameSpeed, [Trusted players] Change the game speed. Use argument \"help\" to see additional information.",
	func = QoL.GameSpeed,
	version = "1.4"
}




function QoL.ListCorpses(event)
	local fake, player = Lib.SafeEventPlayer(event)
	if not player then
		log("Error: Command/NameOfItem: SafePlayer was nil")
		return
	end
	--if fake then
	--	player.print("This command can't be used from the server console.")
	--	return
	--end

	for _, surface in pairs(game.surfaces) do
		local count = 0
		for _, corpse in pairs(surface.find_entities_filtered{name="character-corpse"}) do
			if count == 0 then
				player.print("Corpses for: "..surface.name)
			end
			player.print("    "..game.players[corpse.character_corpse_player_index].name.." corpse is located [gps="..corpse.position.x..","..corpse.position.y.."]")
			count = count + 1
		end
	end
end
myCommands["ListCorpses"] =
{
	help = "[Anyone] List all corpses on the map.",
	func = QoL.ListCorpses,
	version = "1.0"
}

function QoL.On_Console_Command(event)
	local fake, player = Lib.SafeEventPlayer(event)
	local command = event.command
	local params = event.parameters

	if (player == nil or player.admin)
		and command == "c"
		and params:find("^[ ]*game.speed[ ]*=")
	then
		log("Game speed may be changed, adjusting walkspeed")
		QoL.OnGameSpeedChanged()
	end
end

-- Initialisation




function QoL.On_Init()
	if not global.GameSpeedLowerBound or not global.GameSpeedUpperBound then
		local fakePlayer = {
			name = "LeosQoLCommands.On_Init"
		}
		QoL.ResetSpeedBounds(fakePlayer)
	end


	if not global.walkSpeedModifier then
		global.walkSpeedModifier = 1
	end
	log("Registering On_Console_Command")
	script.on_event(defines.events.on_console_command, QoL.On_Console_Command)
end


function QoL.On_Load()
	--log("Registering On_Console_Command")
	script.on_event(defines.events.on_console_command, QoL.On_Console_Command)
end

function QoL.On_Configuration_Changed()
	QoL.On_Init()
end

QoL.ConsoleCommands = myCommands
--log("Fullread")
return QoL