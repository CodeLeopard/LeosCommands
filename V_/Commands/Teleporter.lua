--[[ Copyright (C) 2019  CodeLeopard

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License version 3
	along with this program.  If not, see https://opensource.org/licenses/gpl-3.0
]]



local Lib = require("CommandsLib")

local TeleP = {}
TeleP.Version = "0.0.1"
TeleP.Name = "Teleport Points"

myCommands = {}

local defaultRadius = 5 * 5 -- Radius from Teleport Point in order to teleport. (squared for performance)
local defaultTimeout = 10 * 60 -- ticks


function TeleP.Go(event)
	local fake, player = Lib.SafeEventPlayer(event)
	if not player then
		log("Error: Command/TeleP.Go: SafePlayer was nil")
		return
	end
	if fake then
		player.print("This command can't be used from the server console.")
		return
	end

	local pIndex = event.player_index
	local parameters = event["parameter"]


	--Lib.CheckPermission?

	if not parameters then TeleP.List(event) return end

	local words = Lib.SmartWordSeparation(parameters)
	local name = words[1]

	local tpList = global.Teleporters
	if not tpList then player.print("There are no Teleport points defined.") return end

	local point = tpList[name]
	if not point then player.print("The point " .. name .. " does not exist.") return end


	local lastTP = global.recentTP[pIndex]
	if(lastTP) then
		local time = lastTP + global.TeleporterTimeOut - event.tick
		if ( time > 0) then player.print("Timeout not expired: " .. math.floor(time/60) .. " seconds remaining.") return end
	end


	local closeEnough = false
	for k, v in pairs(tpList) do
		local x, y = player.position.x - v.position.x, player.position.y - v.position.y
		local sqrDist = x * x + y * y
		--player.print(sqrDist)
		if(sqrDist < global.TeleporterRadius) then
			closeEnough = true
			break
		end
	end
	if not closeEnough then player.print("You need to be on a Teleport Point in order to Teleport.") return end

	if not Lib.SafeTP(player, point.position) then
		player.print("Did not find a safe position to teleport to, the Teleport Point may be occupied.")
		return
	end

	global.recentTP[pIndex] = event.tick; -- Set the last tick for this player
end
myCommands["TeleportGo"] =
{
	help = "Go to a teleport destination",
	func = TeleP.Go,
	version = "0.1",
	aliases = { "tpg" }
}



function TeleP.Add(event)
	local fake, player = Lib.SafeEventPlayer(event)
	if not player then
		log("Error: Command/TpPAdd: SafePlayer was nil")
		return
	end
	if fake then
		player.print("This command can't be used from the server console.")
		return
	end

	local parameters = event.parameter

	if not player.admin then player.print("Only Admins can use this command!") return end

	if not parameters then player.print("Supply a name for the Teleport point.") return end

	local tpList = global.Teleporters
	if not tpList then
		tpList = {}
		global.Teleporters = tpList
	end



	local words = Lib.SmartWordSeparation(parameters)
	local name = words[1]
	if tpList[name] then player.print(name .. " already exists!") return end

	tpList[name] = {
		name = name,
		position =
		{
			x = math.floor(player.position.x),
			y = math.floor(player.position.y)
		}
	}
	TeleP.InitPoint(tpList[name])
	player.print("Done.")
end
myCommands["TeleportAdd"] =
{
	help = "[Admin] Add a teleport destination",
	func = TeleP.Add,
	version = "0.1"
}


function TeleP.Remove(event)
	local fake, player = Lib.SafeEventPlayer(event)
	if not player then
		log("Error: Command/TpPRemove: SafePlayer was nil")
		return
	end


	local parameters = event["parameter"]

	if not player.admin then player.print("Onely Admins can use this command!") return end

	if not parameters then player.print("Supply a name for the Teleport point.") return end

	local tpList = global.Teleporters
	if not tpList then
		tpList = {}
		global.Teleporters = tpList
	end

	local words = Lib.SmartWordSeparation(parameters)
	local name = words[1]
	if not tpList[name] then player.print(name .. " does not exist!") return end

	TeleP.RemovePoint(tpList[name])
	tpList[name] = nil
	player.print("Done.")
end
myCommands["TeleportRemove"] =
{
	help = "[Admin] Remove a teleport destination",
	func = TeleP.Remove,
	version = "0.1"
}

function TeleP.InitPoint(point)
	-- do nothing for now.
end

function TeleP.RemovePoint(point)
	-- do nothing for now
end


function TeleP.List(event)
	local fake, player = Lib.SafeEventPlayer(event)
	if not player then
		log("Error: Command/TpPList: SafePlayer was nil")
		return
	end



	local parameters = event["parameter"]


	local verbose = parameters and parameters:lower() == "verbose"

	local tpList = global.Teleporters
	if not tpList then
		tpList = {}
		global.Teleporters = tpList
	end

	for k, v in pairs(tpList) do
		local string =  v.name

		if verbose then string = string .. " @ x" .. v.position.x .. " y" .. v.position.y end

		player.print(string)
	end
end
myCommands["TeleportList"] =
{
	help = "Lists all teleport destinations. Use parameter \"Verbose\" to list positions too.",
	func = TeleP.List,
	version = "0.1",
	aliases = { "tpl" }
}

function TeleP.Setting(event)
	local fake, player = Lib.SafeEventPlayer(event)
	if not player then
		log("Error: Command/TpPSetting: SafePlayer was nil")
		return
	end


	local parameters = event["parameter"]

	if not player.admin then player.print("Onely Admins can use this command!") return end

	if not parameters then player.print("Supply a setting to edit. Settings: Radius, Timeout.") return end

	local words = Lib.SmartWordSeparation(parameters)
	local name = words[1]

	if(name:lower() == "radius") then
		if(not words[2]) then
			player.print("The radius you can be from a Teleport Point in order to use it. Currently: "
			.. math.sqrt( global.TeleporterRadius ) .. " tiles.")
			return
		end
		local num = tonumber(words[2])
		if not num then player.print(words[2] .. " could not be converted to a number.") return end
		global.TeleporterRadius = num * num
		player.print("Radius updated to " .. num .. " tiles.");
		return
	end
	if name:lower() == "timeout" then
		if(not words[2]) then
			player.print("The TimeOut between Teleports. Currently: " .. (global.TeleporterTimeOut/60) .. " seconds.")
			return
		end
		local num = tonumber(words[2])
		if not num then player.print(words[2] .. " could not be converted to a number.") return end
		global.TeleporterTimeOut = num * 60
		player.print("TeleporterTimeOut updated to " .. num .. " seconds.");
		return
	end

	player.print("unknown setting: " .. name)
	return
end
myCommands["TeleportSetting"] =
{
	help = "[Admin] Edit teleporter settings",
	func = TeleP.Setting,
	version = "0.1"
}




function TeleP.On_Init()
	if not global.TeleporterRadius then
		global.TeleporterRadius = defaultRadius
		log("TeleP.On_Init: TeleporterRadius not defined, default is " .. defaultRadius .. " tiles.")
	end
	if not global.TeleporterTimeOut then
		global.TeleporterTimeOut = defaultTimeout
		log("TeleP.On_Init: TeleporterTimeOut not defined, default is " .. defaultTimeout .. " seconds.")
	end
	if not global.recentTP then
		global.recentTP = {}
	end
end

function TeleP.On_Configuration_Changed()
	TeleP.On_Init()
end


TeleP.ConsoleCommands = myCommands
return TeleP