--[[ Copyright (C) 2019  CodeLeopard

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License version 3
    along with this program.  If not, see https://opensource.org/licenses/gpl-3.0
]]


--[[
    Cheat Commands:
        A Couple of variations on Teleportation.


]]

local Lib = require("CommandsLib")


local Cheats = {}
Cheats.Version = "0.2.1"
Cheats.Name = "Cheats"

local myCommands = {}



function Cheats.Exec(event)
	local fake, player = Lib.SafeEventPlayer(event)
	if not player then
		log("Error: Command/Teleport: SafePlayer was nil")
		return
	end
	if not player.admin then player.print("Only Admins can use this command!") return end

	local argument = event.parameter

	-- Hopefully make print, if invoked in the code work as expected.
	local print = player.print

	local resultFunc, error = loadstring(argument, "CommandLine")

	if not resultFunc then
		player.print("Error loading code: "..tostring(error))
		return
	end

	local result = table.pack(pcall(resultFunc))
	local success = result[1]

	if not success then
		player.print("Error running code: "..tostring(result[2]))
		return
	end

	-- Remove success flag.
	table.remove(result, 1)

	if #result > 0 then
		player.print(serpent.block(result))
	else
		-- todo: remove when it works because annoying
		player.print("The code executed successfully and returned no result.")
	end
end
myCommands["Exec"] =
{
	help = "[Admins] Execute arbitrary code.",
	func = Cheats.Teleport,
	version = "1.0",
}





local TP_History;

function Cheats.On_TP_History_Length(event)
	-- For each player: trim the history.
	local maxLength = settings.global["TP_History_Length"].value
	if maxLength == 0 then
		-- Special case shortcut
		global.Cheats_TP_History = {}
		TP_History = global.Cheats_TP_History
		return
	end

	for pIndex, playerHistory in pairs(TP_History) do
		while #playerHistory >= maxLength do
			-- TODO: Bulk remove could be more efficient than this.

			-- remove oldest history item first
			table.remove(playerHistory, 1)
		end
	end
end

function Cheats.PushTPHistory(pIndex, position, surface)
	local maxLength = settings.global["TP_History_Length"].value
	if maxLength == 0 then return end

	local playerHistory = TP_History[pIndex] or {}
	TP_History[pIndex] = playerHistory

	while #playerHistory >= maxLength do
		-- TODO: Performance does not scale well for large history!
		-- TODO: Use circular array.

		-- remove oldest history item first
		table.remove(playerHistory, 1)
	end

	table.insert(playerHistory, {position, surface})
end

function Cheats.PopTPHistory(pIndex)
	local playerHistory = TP_History[pIndex]
	if playerHistory == nil then return nil end

	if #playerHistory == 0 then return nil end

	local item = table.remove(playerHistory)

	return item[1], item[2]
end


function Cheats.Teleport(event)
	local fake, player = Lib.SafeEventPlayer(event)
	if not player then
		log("Error: Command/Teleport: SafePlayer was nil")
		return
	end
	if fake then
		player.print("This command can't be used from the server console.")
		return
	end

	if not player.admin then player.print("Only Admins can use this command!") return end

	local pIndex = event.player_index
	local argument = event.parameter
	local oldPosition = player.position
	local oldSurface = player.surface

	if argument then
		local otherPlayer = game.players[argument]
		if otherPlayer and otherPlayer.valid then
			if not Lib.SafeTP(player, otherPlayer.position, otherPlayer.surface) then
				player.print("Did not find a safe position to teleport to.")
				return
			else
				Cheats.PushTPHistory(pIndex, oldPosition, oldSurface)
			end

			if not game.connected_players[otherPlayer.index] then
				player.print("The given player was not online, this is the position they logged off.")
			end
		else
			player.print("The given player has never been online or was invalid.")
		end
		return
	end

	if player.selected then
		if not Lib.SafeTP(player, player.selected.position) then
			player.print("Did not find a safe position to teleport to.")
			return
		else
			Cheats.PushTPHistory(pIndex, oldPosition, oldSurface)
		end
		return
	else
		player.print("There was nothing selected. Select a entity with the cursor or add a player name to the command.")
		return
	end
end
myCommands["TP"] =
{
	help = "[Admins] Teleports you to the entity you select, or the given player.",
	func = Cheats.Teleport,
	version = "2.0",
}


function Cheats.TeleportBack(event)
	local fake, player = Lib.SafeEventPlayer(event)
	if not player then
		log("Error: Command/Teleport: SafePlayer was nil")
		return
	end
	if fake then
		player.print("This command can't be used from the server console.")
		return
	end

	if not player.admin then player.print("Only Admins can use this command!") return end

	local pIndex = event.player_index
	local argument = event.parameter
	local playerHistory = TP_History[pIndex]

	if not playerHistory then
		player.print("No teleport history.")
		return
	elseif #playerHistory == 0 then
		player.print("Teleport history empty.")
		return
	end

	local position, surface = Cheats.PopTPHistory(pIndex)
	if not position or not surface then 
		player.print("Error fetching teleport history.")
		return 
	end

	if not Lib.SafeTP(player, position, surface) then
		-- Re-insert
		--Cheats.PushTPHistory(pIndex, position, surface)

		player.print("Did not find a safe position to teleport to.")
		return
	end
end
myCommands["TPB"] =
{
	help = "[Admins] Teleports you to where you teleported from.",
	func = Cheats.TeleportBack,
	version = "1.0",
}

function Cheats.Teleport2Me (event)
	local fake, player = Lib.SafeEventPlayer(event)
	if not player then
		log("Error: Command/Teleport2Me: SafePlayer was nil")
		return
	end
	if fake then
		player.print("This command can't be used from the server console.")
		return
	end

	local argument = event.parameter

	if not player.admin then player.print("Only Admins can use this command!") return end

	if argument then
		local otherPlayer = game.players[argument]
		if not game.connected_players[otherPlayer.index] then
			player.print("The given player was not online.")
			return
		end
		if otherPlayer and otherPlayer.valid then
			if not Lib.SafeTP(otherPlayer, player.position, player.surface) then
				player.print("Did not find a safe position to teleport to.")
				return
			end
		else
			player.print("The given player has never been online or was invalid.")
		end
		return
	end

	if player.selected then
		if not Lib.SafeTP(player.selected, player.position, player.surface) then
			player.print("Did not find a safe position to teleport to.")
			return
		end
	else
		player.print("There was nothing selected. Select a entity with the cursor or add a player's name to the command.")
	end
end
myCommands["TP2Me"] =
{
	help = "[Admins] Teleports the thing you have selected to you.",
	func = Cheats.Teleport2Me,
	version = "2.0",
}



function Cheats.Teleport2Spawn (event)
	local fake, player = Lib.SafeEventPlayer(event)
	if not player then
		log("Error: Command/Teleport2Spawn: SafePlayer was nil")
		return
	end
	if fake then
		player.print("This command can't be used from the server console.")
		return
	end

	if not player.admin then player.print("Only Admins can use this command!") return end

	player.teleport(player.force.get_spawn_position(player.surface))
end
myCommands["TP2Spawn"] =
{
	help = "[Admins] Teleports you to the exact position of spawn.",
	func = Cheats.Teleport2Spawn,
	version = "2.0",
}


function Cheats.On_Init()
	if not global.Cheats_TP_History then
		log("Create: global.Cheats_TP_History")
		global.Cheats_TP_History = {}
	end
	TP_History = global.Cheats_TP_History
end

function Cheats.On_Load()
	TP_History = global.Cheats_TP_History
end

function Cheats.On_Configuration_Changed()
	Cheats.On_Init()
end


Cheats.ConsoleCommands = myCommands
return Cheats