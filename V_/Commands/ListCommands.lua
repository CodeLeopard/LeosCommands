--[[ Copyright (C) 2019  CodeLeopard

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License version 3
    along with this program.  If not, see https://opensource.org/licenses/gpl-3.0
]]


local Lib =     require("CommandsLib")

local ListComm = {}
ListComm.Version = "0.0.2"
ListComm.Name = "List"

local myCommands = {}


--Todo: Refactor to a generic list editor
function ListComm.CommandRules(event)
	local fake, player = Lib.SafeEventPlayer(event)
	if not player then
		log("Error: Command/CommandRules: SafePlayer was nil")
		return
	end


	local inputString = event.parameter


	local ruleList = global.RuleList
	if not ruleList then
		global.RuleList = {}
		ruleList = global.RuleList
		player.print("There are no rules defined yet.")

		if player.admin then
			player.print("To edit the rules see \"/rules help\"")
		end
		return
	end

	if not inputString then
		player.print("Rules and Notices:")
		for i = 1, #ruleList do
			player.print("#" .. i .. " " .. ruleList[i])
		end
		return
	end

	if not player.admin then player.print("You do not have permission to supply arguments to this comand.") return end

	if inputString == "help" then
		player.print("Arguments: 1 ==> \"clear\": Clears the list of rules.")
		player.print("Arguments: 2 ==> \"add\": supply a \"quoted rule\" to be added.")
		player.print("Arguments: 2 ==> \"remove\": supply a index of the rule to be removed.")
		player.print("Arguments: 3 ==> \"replace\": supply a index of the rule to be replaced and a \"quoted rule\" for the new value.")
		player.print("Arguments: 3 ==> \"insert\": supply a index of the position for insertion and a \"quoted rule\" to be inserted.")
		return
	end

	if inputString == "clear" then
		global.RuleList = {}
		player.print("Rules have been cleared")
		return
	end

	local arguments = Lib.SmartWordSeparation(inputString)
	if not arguments[1] then player.print("Something went wrong parsing your input.") return end

	log(player.name .. " edited the rules with argument(s): \"" .. inputString .. "\"")

	if arguments[1] == "add" then
		if not arguments[2] then player.print("You need to supply a \"quoted rule\" to this command") return end
		table.insert(global.RuleList, arguments[2])
		player.print("Rule was added.")
		return
	end

	local ruleNo = tonumber(arguments[2])
	if not ruleNo then player.print("Your rule number argument could not be converted to a number.") return end

	if arguments[1] == "remove" then
		if not global.RuleList[ruleNo] then player.print("There is no rule #" .. ruleNo) return end
		table.remove(global.RuleList, ruleNo)
		player.print("Rule #" .. ruleNo .. " was removed.")
		return
	end

	if not arguments[3] then player.print("you need to supply a rule as 3rd argument.") return end

	if arguments[1] == "replace" then
	if not global.RuleList[ruleNo] then player.print("There is no rule #" .. ruleNo) return end
		global.RuleList[ruleNo] = arguments[3]
		player.print("Rule #" .. ruleNo .. " was replaced.")
		return
	end

	if arguments[1] == "insert" then
		--[[In array global[][], at position, insert this. (And move the item that was at position down, etc.)]]
		--[[And yes re-ordering the arguments when you add one is a good idea #LUALogic]]
		if not global.RuleList[ruleNo] then player.print("There is no rule #" .. ruleNo) return end
		table.insert(global.RuleList, arguments[2], arguments[3])
		player.print("Rule was inserted as #" .. ruleNo .. ".")
		return
	end

	player.print("Unknown argument(s): \"".. inputString.."\".")
end
myCommands["Rules"] =
{
	help = "Shows the rules on this server. [Admin] edit the rules.",
	func = ListComm.CommandRules,
	version = "1.4"
}







ListComm.ConsoleCommands = myCommands
return ListComm