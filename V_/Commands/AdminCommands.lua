--[[ Copyright (C) 2019  CodeLeopard

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License version 3
    along with this program.  If not, see https://opensource.org/licenses/gpl-3.0
]]


local Lib =     require("CommandsLib")

local Admin = {}
Admin.Version = "0.2.1"
Admin.Name = "Admin"

local myCommands = {}

function Admin.SpyMode(event)
	local fake, player = Lib.SafeEventPlayer(event)
	if not player then
		log("Error: Command/SpyMode: SafePlayer was nil")
		return
	end
	if fake then
		player.print("This command can't be used from the server console.")
		return
	end
	local pIndex = event.player_index
	local parameters = event["parameter"]

	if not player.admin then player.print("You do not haver permission to use this command!") return end


	if not global.AdminSpectators then log("Command.SpyMode.AdminSpectators was nil") global.AdminSpectators = {} end

	log("Command.SpyMode: " .. serpent.block(event))

	if not parameters or parameters == "" then
		if player.character then --Make ghost
			player.character.destructible = false
			player.walking_state.walking = false

			global.AdminSpectators[pIndex] =
			{
				character = player.character,
				--force = player.force,
			}

			player.set_controller { type = defines.controllers.god }
			player.cheat_mode = true
			player.print("Beware! Any items you have in your spectator inventory are lost when you return to your character!")

		else --return to character
			local spectatorData = global.AdminSpectators[pIndex]
			if spectatorData then
				player.set_controller { type = defines.controllers.character, character = spectatorData.character }
				global.AdminSpectators[pIndex] = nil
				player.character.destructible = true
			else
				player.print("We don't know about any character to put you back into.")
				player.print("Use subcommand \"help\" for information.")
				--global.AdminSpectators[pIndex] = nil
			end

			player.cheat_mode = false
		end
		return
	end


	if parameters == "IntoMouseSelect" or parameters == "intomouseselect" then
		local sel = player.selected

		if player.character then player.print("You already have a character.") return end
		if not sel  then player.print("There is nothing selected.") return end
		if not sel.prototype.name == "player" then player.print("The selected thing is not a character.") return end
		if sel.player then player.print("That character belongs to another player.") return end

		-- there was nothing wrong.
		player.set_controller { type = defines.controllers.character, character = player.selected }
		global.AdminSpectators[player] = nil

		player.character.destructible = true
		player.cheat_mode = false
		return
	end

	if parameters == "CreateNew" or parameters == "createnew" then
		if player.character then player.print("You already have a character.") return end

		player.set_controller
			{
				type = defines.controllers.character,
				character = player.surface.create_entity
					{
						name = "player",
						position = { 0, 0 },
						force = global.AdminSpectators[player] or player.force
					}
			}
		global.AdminSpectators[player] = nil
		player.cheat_mode = false
		return
	end
end
myCommands["SpyMode"] =
{
	help = "[Admins] Toggle SpyMode, use subcommand \"help\" for more info.",
	func = Admin.SpyMode,
	version = "1.0",
}


function Admin.CommandPermissions(event)
	local fake, player = Lib.SafeEventPlayer(event)
	if not player then
		log("Error: Command/CommandPermissions: SafePlayer was nil")
		return
	end

	local parameters = event["parameter"]

	log("CommandPermissions: EventDatas:\n" .. serpent.block(event))


	Lib.ResetPermissionGroups() --[[Will check if the groups are valid, and reset them if they are not.]]

	if not parameters then
		player.print("The groups that have permission to use privileged commands are:")
		player.print("\"/promoted\" Admins, use command \"/admins\" to see the Admins.")
		for key, val in pairs(global.ElevatedPermissionGroups) do
			player.print(key)
		end
		return
	end

	--[[If parameters ware given:]]
	if not Lib.CheckPermission(player) then player.print("You do not have permission to edit the permissions.") return end

	local parametersAsWords = Lib.SmartWordSeparation(parameters)

	local subCommand = parametersAsWords[1]
	if not subCommand then player.print("Something went wrong parsing your input: a SubCommand was not found!") return end

	if subCommand == "help" then
		player.print("IMPORTANT: renaming a permissiongroup from the GUI will disallow it from LeosCommands.")
		player.print("empty subCommand: show the current configuration.")
		player.print("subCommand \"clear\": clears all permission groups from the privileged list.")
		player.print("subCommand \"add\" or \"remove\": use the subcommand whitout any arguments to see their helpText.")
		return
	end


	if subCommand == "clear" then
		global.ElevatedPermissionGroups = {}
		player.print("Cleared all permission groups from priviledged permission access. Onely \"/promoted\" Admins will have access.")
		return
	end

	if subCommand == "add" then
		if not parametersAsWords[2] then
			player.print("Provide the name(s) \"in quotes\" of the permission group(s) to add.")
			return
		else
			for i=2, #parametersAsWords do
				local groupName = parametersAsWords[i]
				if not groupName then
					player.print("Something went wrong iterating your parameters, check /commandpermissions to see what has been changed.")
				elseif game.permissions.get_group(groupName) then
					global.ElevatedPermissionGroups[groupName] = 1
					player.print(groupName .. " is now a privileged group.")
				else
					player.print(groupName .. " is not a valid permission group, it will not be added!")
				end
			end
		end
		return
	end

	if subCommand == "remove" then
		if not parametersAsWords[2] then
			player.print("Provide the name(s) \"in quotes\" of the permission group(s) to remove.")
			return
		else
			for i=2, #parametersAsWords do
				local groupName = parametersAsWords[i]
				if not groupName then
					player.print("Something went wrong iterating your parameters, check /commandpermissions to see what has been changed.")
				else
					global.ElevatedPermissionGroups[groupName] = nil
					player.print(groupName .. " is no longer a privileged group.")
				end
			end
		end
		return
	end

	--[[SubCommand was not found:]]
	player.print("Unknown subCommand: \""  .. subCommand .. "\"  try \"/commandpermissions help\" for help.")
end
myCommands["CommandPermissions"] =
{
	help = "[Admins] Change wich permission groups have access to privileged commands. Use subcommand \"help\" for additional info.",
	func = Admin.CommandPermissions,
	version = "1.4",
}


function Admin.Inventory(event)
	local fake, player = Lib.SafeEventPlayer(event)
	if not player then
		log("Error: Command/Inventory: SafePlayer was nil")
		return
	end
	if not player.admin then player.print("Only Admins can use this command!") return end

	local argument = event.parameter

	if argument then
		local otherPlayer = game.players[argument]
		if otherPlayer and otherPlayer.valid then
			local invTypes = { -- Player inventories
				main = defines.inventory.player_main,
				guns = defines.inventory.player_guns,
				ammo = defines.inventory.player_ammo,
				armor = defines.inventory.player_armor,
				trash = defines.inventory.player_trash
			}
			local stacks = 0
			for name, def in pairs(invTypes) do
				local inv = otherPlayer.get_inventory(def)
				if inv ~= nil then
					player.print("Inv: " .. name .. ":")
					for stack in inv do
						player.print("* " .. stack.name .. stack.count)
						stacks = stacks + 1
					end
				end
			end
			if stacks > 10 then
				player.print("You may need to check the logfile to see all items.")
			end
			if stacks == 0 then
				player.print("Empty inventory.")
			end
		else
			player.print(argument .. " was not found.")
		end
	else
		player.print("You need to supply a player name.")
	end
end
myCommands["SCInventory"] =
{
	help = "[Server Console] List the inventory of a player. Note: in-game Admins should use /open",
	func = Admin.Inventory,
	version = "1.0",
}

function Admin.On_Init()
	if not global.ElevatedPermissionGroups then
		Lib.ResetPermissionGroups(true) --Will force resetting the permission groups.
	end
end

--[[
function Admin.On_Load()
	
end
]]--

function Admin.On_Configuration_Changed()
	Admin.On_Init()
end

Admin.ConsoleCommands = myCommands
return Admin