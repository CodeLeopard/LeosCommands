--[[ Copyright (C) 2019  CodeLeopard

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License version 3
	along with this program.  If not, see https://opensource.org/licenses/gpl-3.0
]]

function safeRequire(moduleName)
	local status, module = pcall(require, moduleName)
	return status and module or nil;
end


local Lib    = require("Commands.CommandsLib")
local Admin  = require("Commands.AdminCommands")
local QoL    = require("Commands.QoLCommands")
local Cheats = require("Commands.CheatCommands")
local TeleP  = require("Commands.Teleporter")
local ListC  = require("Commands.ListCommands")


local LeosCommands = {}
LeosCommands.Version = "--==##VERSION.MAIN##==--" -- << Set automatically by build script.
LeosCommands.Name = "LeosCommands"

LeosCommands.Modules =
{
	Control = LeosCommands,
	Lib 	= Lib 	 or nil,
	Admin 	= Admin  or nil,
	QoL 	= QoL 	 or nil,
	Cheats 	= Cheats or nil,
	TeleP 	= TeleP  or nil,
	ListC 	= ListC  or nil,
}

local myCommands = {}

if not Lib then log("LeosCommandsLib not found: unable to proceed.") return end

function LeosCommands.CommandVersion(event)
	local fake, player = Lib.SafeEventPlayer(event)
	if not player then
		log("Error: Command/CommandVersion: SafePlayer was nil")
		return
	end

	for moduleName, Module in pairs(LeosCommands.Modules) do
		player.print("* " .. moduleName .. ": v" .. Module.Version)

		for commandName, commandContainer in pairs(Module.ConsoleCommands) do
			player.print("* * /" .. commandName .. ": v" .. commandContainer.version)
		end
	end
end
myCommands["LeosCommVersion"] =
{
	help = "Displays the versions of LeosCommand Modules that are in use.",
	func = LeosCommands.CommandVersion,
	version = "1.1"
}


function LeosCommands.CommandHelp(event)
	local fake, player = Lib.SafeEventPlayer(event)
	if not player then
		log("Error: Command/CommandHelp: SafePlayer was nil")
		return
	end
	player.print("HelpText Not Implemented")
end
--[[
myCommands["LeosCommHelp"] =
{
	help = "Gives detailed explanation of Leopard's Command Suite",
	func = LeosComm.CommandHelp,
	version = "0.1"
}
]]


function LeosCommands.BrokenCommand(event)
	local fake, player = Lib.SafeEventPlayer(event)
	if not player or not player.admin then
		return
	end

	player.print(1 + nil)
end

local enableBroken = false
if enableBroken then
	myCommands["BrokenComm"] =
	{
		help = "Command with intentional error",
		func = LeosCommands.BrokenCommand,
		version = "0.1"
	}
end

LeosCommands.ConsoleCommands = myCommands
-- ########### No Console commands may be defined beyond this point! ###########

local settingStrToModule = {}
settingStrToModule["Module-" .. Admin.Name	] = Admin
settingStrToModule["Module-" .. Cheats.Name	] = Cheats
settingStrToModule["Module-" .. QoL.Name	] = QoL
settingStrToModule["Module-" .. TeleP.Name	] = TeleP


function LeosCommands.On_Runtime_Setting_Changed(event)
	local player = game.players[event.player_index]
	local settingStr = event.setting

	if settingStr == "Adjust-walkspeed-to-GameSpeed" then
		QoL.OnGameSpeedChanged()
		return
	end

	if settingStr == "TP_History_Length" then
		Cheats.On_TP_History_Length(event)
		return
	end

	local module = settingStrToModule[settingStr]

	if(module == nil) then
		log("Unknown Setting: " .. settingStr)
		return
	end

	local set = settings.global[settingStr]
	local value = set == nil or set.value

	local text = ""
	if value then text = " enabled." else text = " disabled." end

	log(settingStr .. text)

	if value then
		LeosCommands.RegisterModuleCommands(module)
	else
		LeosCommands.UnregisterModuleCommands(module)
	end


	if(player) then
		player.print("Commands from " .. settingStr .. text)
	end
end



function LeosCommands.RegisterModuleCommands(module)
	for commandName, commandContainer in pairs(module.ConsoleCommands) do
		LeosCommands.AddCommand(commandName, commandContainer)
	end
end


function LeosCommands.UnregisterModuleCommands(module)
	for commandName, commandData in pairs(module.ConsoleCommands) do
		local name = commandName:lower()
		commands.remove_command(name)

		if commandData.aliases then
			for _, value in pairs(commandData.aliases) do
				commands.remove_command(value:lower())
			end
		end
	end
end



local registercommands_counter = 0

function LeosCommands.RegisterCommands()
	log("Registering On_Runtime_Setting_Changed")
	script.on_event(defines.events.on_runtime_mod_setting_changed, LeosCommands.On_Runtime_Setting_Changed)

	registercommands_counter = 0

	local settingsGlobal = settings.global

	log("Registering Commands...")
	for moduleName, module in pairs(LeosCommands.Modules) do
		local moduleSetting = settingsGlobal["Module " .. module.Name]
		if moduleSetting == nil or moduleSetting.value then
			log("* for Module: " .. moduleName)
			LeosCommands.RegisterModuleCommands(module)
		else
			log("Module " .. module.Name .. " is disabled.")
		end
	end
	log("Registered " .. registercommands_counter .. " commands.")
end

--[[AddCommand (version 1.0)]]
function LeosCommands.AddCommand(name, commandData)
	local commandName = name:lower()
	local commandHelp = commandData["help"]
	local commandFunc = commandData["func"]

	commandFunc = SafeCommandWrapper(commandFunc, name)


	local aliasCount = 0

	if commandData.aliases then
		for _, value in pairs(commandData.aliases) do
			local alias = value:lower()
			commands.remove_command(alias)
			commands.add_command(alias, commandHelp, commandFunc)
			aliasCount = aliasCount + 1
		end
	end

	commands.remove_command(commandName)
	commands.add_command(commandName, commandHelp, commandFunc)

	registercommands_counter = registercommands_counter + 1

	if aliasCount == 0 then
		log("* * /" .. commandName)
	else
		log("* * /" .. commandName .. " + " .. aliasCount .. " aliases")
	end
end

function SafeCommandWrapper(functio, name)
	return function(event)
		local _event = event
		local fake, player = Lib.SafeEventPlayer(event)
		local function errH(e)
			if name == nil then name = "unknown" end
			log("Error executing: /" .. name .. "\nEventData= " .. serpent.block(_event) .. "\nErrorTrace=" .. serpent.block(e))
			if player then
				player.print("An error ocurred while executing: /" .. name)
			end
		end
		xpcall(functio, errH, event)
	end
end

--[[On_Init (version 1.2)]]
local function On_Init()
	log("On_Init")

	for name, module in pairs(LeosCommands.Modules) do
		local func = module.On_Init
		if func then
			func()
		end
	end

	LeosCommands.RegisterCommands()
end

--[[On_Load (version 1.2)]]
local function On_Load()
	log("On_Load")

	for name, module in pairs(LeosCommands.Modules) do
		local func = module.On_Load
		if func then
			func()
		end
	end

	LeosCommands.RegisterCommands()
end

--[[On_Configuration_Changed (version 1.0)]]
local function On_Configuration_Changed()
	log("On_Configuration_Changed")

	for name, module in pairs(LeosCommands.Modules) do
		local func = module.On_Configuration_Changed
		if func then
			func()
		end
	end
end



--[[First load]]
script.on_init(On_Init)

--[[Every load, no acess to global!]]
script.on_load(On_Load)

script.on_configuration_changed(On_Configuration_Changed)


--log("Fullread")
return LeosCommands





